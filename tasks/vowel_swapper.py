def vowel_swapper(string):
    # ==============
    my_string = ''

    subs = {'a': '4',
            'e': '3',
            'i': '!',
            'o': 'ooo',
            'O': '000',
            'u': '|_|'}

    for char in string:
        if char == 'o' or char == 'O':
            my_string += subs[char]
        else:
            my_string += subs.get(char.lower(), char)

    return my_string


    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console