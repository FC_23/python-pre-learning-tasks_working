def vowel_swapper(string):
    # ==============
    my_string = ''

    # dict.get function below will add all distinct chars to 'vowel_counter', but vowels pre-added for clarity.

    vowel_counter = {'a': 0,
                     'e': 0,
                     'i': 0,
                     'o': 0,
                     'u': 0}

    subs = {'a': '4',
            'e': '3',
            'i': '!',
            'o': 'ooo',
            'O': '000',
            'u': '|_|'}

    vowels = 'aeiou'

    for char in string:
        lc = char.lower()
        if lc in vowels:
            vowel_counter[lc] += 1
        if vowel_counter.get(lc, 0) == 2:
            if char == 'O' or char == 'o':
                my_string += subs[char]
            else:
                my_string += subs[lc]
        else:
            my_string += char

    print(vowel_counter)
    return my_string

    # ==============


print(vowel_swapper("aAa eEe iIi oOo uUu")) # "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
