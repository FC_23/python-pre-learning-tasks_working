def calculator(a, b, operator):
    # ==============
    if operator == '+':
        ans = a + b
    elif operator == '-':
        ans = a - b
    elif operator == '*':
        ans = a * b
    elif operator == '/':
        ans = int(a / b)

    binary_ans = bin(ans)
    minus_prefix = binary_ans[binary_ans.index('b')+1:]

    return int(minus_prefix)

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
